#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* This is a coin flipping simulator. Enter in how many times you wan to flip the coin and the program will outpu the results. */

#define HEADS 0
#define TAILS 1

void flip(int count)
{
    int toss;

    for(int i = 0; i < count; i++) {

        toss = rand() % 2; 

        if (toss == HEADS) {
            printf("Heads\n");
        }

        if (toss == TAILS) {
            printf("Tails\n");
        }
    }
}

void main()
{ 
    int count;

    printf("Enter desired number of coin flips: ");
    scanf("%d", &count);
    system("clear");
    flip(count);
}
